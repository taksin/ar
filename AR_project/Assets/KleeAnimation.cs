﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KleeAnimation : MonoBehaviour
{
    Animator _animator;
    public AudioSource JumpSound;
    public AudioSource Please;
    public AudioSource HelloSound;
    void Start()
    {
        
    }
    void Update()
    {
        _animator = this.GetComponent<Animator>();
        if (voiceklee.instant.RandomAni == 0)
        {
            Idle();
        }
        if (voiceklee.instant.RandomAni == 1)
        {
            Jump();
        }
        if (voiceklee.instant.RandomAni == 2)
        {
            Plz();
        } 
        if (voiceklee.instant.RandomAni == 3)
        {
            ByeBye();
        }
    }
    void Jump()
    {
        _animator.SetBool("Jump",true);
        voiceklee.instant.RandomAni = 0;
        JumpSound.Play();
    }
    void Plz()
    {
        _animator.SetBool("Plz",true);
        voiceklee.instant.RandomAni = 0;
        Please.Play();
    }
    void ByeBye()
    {
        _animator.SetBool("Bye",true);
        voiceklee.instant.RandomAni = 0;
        HelloSound.Play();
    }

    void Idle()
    {
        _animator.SetBool("Jump",false);
        _animator.SetBool("Plz",false);
        _animator.SetBool("Bye",false);
    }
}
